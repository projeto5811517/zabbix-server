
ARG ZBX_SO=ubuntu
ARG ZBX_VERSION=6.4.12
FROM zabbix/zabbix-server-pgsql:${ZBX_SO}-${ZBX_VERSION}

# User root apenas para build
USER 0

RUN apt update && apt install net-tools telnet tzdata curl vim -y && \
    rm -rf /etc/localtime && \
    ln -s /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && \
    apt install -y unixodbc-dev unixodbc

# Repo ODBC Microsoft
RUN curl https://packages.microsoft.com/keys/microsoft.asc | tee /etc/apt/trusted.gpg.d/microsoft.asc && \ 
    curl https://packages.microsoft.com/config/ubuntu/$(cat /etc/*release | grep VERSION_ID= | cut -d "\"" -f 2)/prod.list | tee /etc/apt/sources.list.d/mssql-release.list && \
    apt update && ACCEPT_EULA=Y apt-get install -y msodbcsql18
    #echo 'export PATH="$PATH:/opt/mssql-tools18/bin"' >> ~/.bashrc

USER 1997